Address	:	1712 19th Street Suite 220

Bakersfield, CA 93301

Phone	:	(661) 793-7222

Website	:	https://www.bakersfieldcriminaldefense.com/

Being arrested for an offense in Kern County can be frightening, particularly if you face fines and incarceration. The legal team at Koenig Law Office is ready to be your first line of defense. You are presumed innocent until proven guilty, and irrespective of the charge you are facing, we will ensure your rights are protected.

Our mission is to enable you to feel ready and comfortable to face the criminal charge against you. With many years of legal experience, you can be confident that you have world-class representation throughout the criminal process.

Dedicated to Defending Your Constitutional Rights Following your arrest, you should not say anything to law enforcement until you have talked to your defense attorney. While you have a right to remain silent, most people don't know their constitutional rights and might not realize that prosecutors could use their words against them in a court of law.

Additionally, the prosecutor should prove beyond any reasonable doubt that you violated the law. However, prosecutors often recommend the maximum penalties, knowing that the jury view accusations with unforgiving eyes. Irrespective of whether you are charged with a felony or misdemeanor, you require the help of a skilled attorney. While the case could be settled out of court, the lawyer should be prepared to defend you in court, and at Koenig Law Office, we are up to the challenge.

Keywords:	Law, Lawyer at Bakersfield, CA. DUI at Bakersfield, CA. Bakersfield, CA Attorney, Criminal Defense law at Bakersfield, CA

Hour	:	24/7

Payment	:	cash, credit/debit cards
